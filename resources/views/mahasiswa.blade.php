<h2>{{ $judul }}</h2>
<a href="/mahasiswa/tambah">ADD</a>
<table>
    <tr>
        <td>NIS</td>
        <td>Nama</td>
        <td>Kelas</td> 
        <td>Action</td>       
    </tr>
    @foreach($mhs as $m)
    <tr>
        <td>{{ $m['nim'] }}</td>
        <td>{{ $m['nama'] }}</td>
        <td>{{ $m['kelas'] }}</td> 
        <td>
            <a href="/mahasiswa/hapus/{{ $m['nim'] }}">Delete</a> | 
            <a href="/mahasiswa/edit/{{ $m['nim'] }}">Edit</a>
        </td>        
    </tr> 
    @endforeach   
</table>
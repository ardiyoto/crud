<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MMahasiswa extends Model
{
    protected $table = "mahasiswa";
    protected $fillable = ['nim','nama','kelas'];
    public $timestamps = false;
}

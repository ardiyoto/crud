<!-- Nampilkan Error -->
@if ($errors->any())
    @foreach($errors->all() as $err)
        <p>{{ $err }}</p>
    @endforeach
@endif

<form method="post" action="/mahasiswa/update">
    @csrf
    <p>
        Nim {{ $mhs['nim'] }}<br/>
        <input type="hidden" name="nim" value="{{ $mhs['nim'] }}">
    </p>
    <p>
        Nama<br/>
        <input type="text" name="nama" value="{{ $mhs['nama'] }}">
    </p>
    <p>
        Kelas<br/>
        <input type="text" name="kelas" value="{{ $mhs['kelas'] }}">
    </p> 
    <p>
        <input type="submit" value="SIMPAN">
    </p>       
</form>
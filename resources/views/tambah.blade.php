<!-- Nampilkan Error -->
@if ($errors->any())
    @foreach($errors->all() as $err)
        <p>{{ $err }}</p>
    @endforeach
@endif


<form method="post" action="/mahasiswa/simpan">
    @csrf
    <p>
        Nim<br/>
        <input type="text" name="nim">
    </p>
    <p>
        Nama<br/>
        <input type="text" name="nama">
    </p>
    <p>
        Kelas<br/>
        <input type="text" name="kelas">
    </p> 
    <p>
        <input type="submit" value="SIMPAN">
    </p>       
</form>
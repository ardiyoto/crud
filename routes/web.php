<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/mahasiswa','MahasiswaControl@index');
Route::get('/mahasiswa/tambah','MahasiswaControl@tambah');
Route::post('/mahasiswa/simpan','MahasiswaControl@simpan');
Route::get('/mahasiswa/hapus/{nim}','MahasiswaControl@hapus');
Route::get('/mahasiswa/edit/{nim}','MahasiswaControl@edit');
Route::post('/mahasiswa/update','MahasiswaControl@update');
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MMahasiswa;

class MahasiswaControl extends Controller
{
    function index(){
        $mhs = MMahasiswa::all();
        $judul = "Data Mhs";
        return view('mahasiswa',compact('mhs','judul'));
    }

    function tambah(){
        return view('tambah');
    }

    function simpan(Request $req){
        //Validate or Rules
        $req->validate([
            'nim' => 'required|max:3',
            'nama' => 'required',
            'kelas' => 'required|max:3'
        ]);

        // Preparation
        $mhs = new MMahasiswa([
            'nim' => $req->get('nim'),
            'nama' => $req->get('nama'),
            'kelas' => $req->get('kelas')
        ]);

        $mhs->save(); // Eksekusi simpan ke tabel

        return redirect('mahasiswa');
    }

    function hapus($nim){
        $mhs = MMahasiswa::where("nim",$nim);
        $mhs->delete();

        return redirect('mahasiswa');
    }

    function edit($nim){
        $mhs = MMahasiswa::where("nim",$nim)->first();
        return view("edit",compact("mhs"));
    }

    function update(Request $req){

         // Preparation
        $mhs = MMahasiswa::where("nim",$req->get("nim"));

        $mhs->update($req->except('_token')); // Eksekusi simpan ke tabel

        return redirect('mahasiswa');       
    }
}
